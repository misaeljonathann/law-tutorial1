from django.shortcuts import render
import requests

def home(request):
    headers = {
        'accept': "application/json",
        'content-type': "application/json"
    }
    response = requests.get('http://freegeoip.app/json/', headers = headers)
    geodata = response.json()
    print(geodata)
    return render(request, 'core/home.html', {
        'ip': geodata['ip'],
        'country': geodata['country_name']
    })